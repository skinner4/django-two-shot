from django.forms import ModelForm
from receipts.models import Account, Receipt
from django import forms

class AccountSignUpForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
        )

class SignUpForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput)
    password_confirmation = forms.CharField(
    max_length=150, widget=forms.PasswordInput)

# class ReceiptForm(forms.ModelForm):
#     class Meta:
#         model = Receipt
#         fields = [
#             'vendor',
#             'total',
#             'tax',
#             'date',
#             'category',
#             'account']
