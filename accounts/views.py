from django.shortcuts import render, redirect
from django.contrib.auth import login
from django.contrib.auth.models import User
from accounts.forms import AccountSignUpForm, SignUpForm
from django.contrib.auth import authenticate, logout

def Login(request):
    if request.method == "POST":
        form = AccountSignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(
                request,
                username=username,
                password=password,
            )
            if user is not None:
                login(request, user)
                return redirect("home_page")
    else:
        form = AccountSignUpForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)

def user_logout(request):
    logout(request)
    return redirect("login")

def sign_up (request):
    if request.method == "POST" :
        form = SignUpForm (request.POST)
        if form.is_valid():
            username = form. cleaned_data[ 'username']
            password = form. cleaned_data[ 'password']
            password_confirmation = form. cleaned_data['password_confirmation']
            # first_name = form.cleaned_data['first_name']
            # last_name = form.cleaned_data[ 'last_name']
            if password == password_confirmation:
                user = User.objects.create_user(
                    username,
                    password=password,
                    # first_name=first_name,
                    # last_name=last_name,
                )
                login (request, user)
                return redirect ("home")
            else:
                form. add_error("password", "Passwords do not match")
    else:
        form = SignUpForm ()
    context = {
        "form": form,
    }
    return render(request, "accounts/signup.html", context)

# def create_receipt(request):
#     if request.method == 'POST':
#         form = ReceiptForm(request.POST)
#         if form.is_valid():
#             form.save()
#             return redirect("receipts")
#     else:
#         form = ReceiptForm()
#     return render(request, "receipts/create_receipt.html", {"form": form})
